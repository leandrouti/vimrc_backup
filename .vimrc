"enables syntax highlighting
syntax on

"enables code identation
set smartindent 

"show line numbers on the left
set number

"Auto indent character spaces
set shiftwidth=3

"quantity of characters spaces when hit tab
set tabstop=3
